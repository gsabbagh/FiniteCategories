# Revision history for FiniteCategories

## 0.1.0.0 -- 2022-03-21

* First version.

## 0.2.0.0 -- 2023-03-13

* Separation between Category and FiniteCategory typeclasses, new architecture for the project, Kan extensions, etc.

## 0.3.0.0 -- 2023-09-29

* Splitting the package with FiniteCategoriesGraphViz. Colimit of composition graphs.

### 0.3.0.1 -- 2023-09-29

* Broadening range of text bounds.

## 0.4.0.0 -- 2023-10-10

* Broadening range of base bounds.

## 0.5.0.0 -- 2023-10-10

* Better bounds.

## 0.6.0.0 -- 2024-01-11

* Sketches, (co)complete categories and cartesian closed categories.

## 0.6.0.1 -- 2024-01-14

* Correct bug in completeDiagram.

## 0.6.0.2 -- 2024-01-22

* Bug fix in checkNaturalTransformation.

## 0.6.1.0 -- 2024-01-23

* Add checkFiniteSketch to be more specific.

## 0.6.1.1 -- 2024-01-23

* Bug fix in sketchMorphism

## 0.6.2.0 -- 2024-01-24

* Add mapOnObjects2 and mapOnArrows2

## 0.6.3.0 -- 2024-02-05

* Add helpers for cones and cocones

## 0.6.3.1 -- 2024-03-06

* Bug fixes in helpers for cones and cocones

## 0.6.4.0 -- 2024-03-08

* Adding leg getter in CartesianClosedCategory

## 0.6.5.0 -- 2024-05-16

* Changes to simplification of CGMorphism

## 0.6.5.1 -- 2024-05-31

* Relax container version bounds