{-# LANGUAGE MultiParamTypeClasses #-}

{-| Module  : FiniteCategories
Description : This file exports all categories.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

This file exports all categories.
-}

module Math.Categories (
    module Math.Categories.Omega,
    module Math.Categories.OrdinalCategory,
    module Math.Categories.TotalOrder,
    module Math.Categories.Galaxy,
    module Math.Categories.FinSet,
    module Math.Categories.FinGrph,
    module Math.Categories.Opposite,
    module Math.Categories.FinCat,
    module Math.Categories.FunctorCategory,
    module Math.Categories.CommaCategory,
    module Math.Categories.ConeCategory,
    module Math.Categories.PresheafCategory,
    module Math.Categories.FinSketch,
) where
    import  Math.Categories.Omega
    import  Math.Categories.OrdinalCategory
    import  Math.Categories.TotalOrder
    import  Math.Categories.Galaxy
    import  Math.Categories.FinSet
    import  Math.Categories.FinGrph
    import  Math.Categories.Opposite
    import  Math.Categories.FinCat
    import  Math.Categories.FunctorCategory
    import  Math.Categories.CommaCategory
    import  Math.Categories.ConeCategory
    import  Math.Categories.PresheafCategory
    import  Math.Categories.FinSketch