{-# LANGUAGE MultiParamTypeClasses #-}

{-| Module  : FiniteCategories
Description : This file exports all finite categories.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

This file exports all finite categories.
-}

module Math.FiniteCategories (
    module Math.FiniteCategories.NumberCategory,
    module Math.FiniteCategories.DiscreteCategory,
    module Math.FiniteCategories.FullSubcategory,
    module Math.FiniteCategories.Hat,
    module Math.FiniteCategories.V,
    module Math.FiniteCategories.Parallel,
    module Math.FiniteCategories.Square,
    module Math.FiniteCategories.Ens,
    module Math.FiniteCategories.Opposite,
    module Math.FiniteCategories.FunctorCategory,
    module Math.FiniteCategories.CompositionGraph,
    module Math.FiniteCategories.SafeCompositionGraph,
    module Math.FiniteCategories.CommaCategory,
    module Math.FiniteCategories.One,
    module Math.FiniteCategories.ConeCategory,
    module Math.FiniteCategories.Subcategory,
    module Math.FiniteCategories.DiscreteTwo,
    module Math.FiniteCategories.LimitCategory,
    module Math.FiniteCategories.ColimitCategory,
    module Math.FiniteCategories.ExponentialCategory,
) where
    import Math.FiniteCategories.NumberCategory
    import Math.FiniteCategories.DiscreteCategory
    import Math.FiniteCategories.FullSubcategory
    import Math.FiniteCategories.Hat
    import Math.FiniteCategories.V
    import Math.FiniteCategories.Parallel
    import Math.FiniteCategories.Square
    import Math.FiniteCategories.Ens
    import Math.FiniteCategories.Opposite
    import Math.FiniteCategories.FunctorCategory
    import Math.FiniteCategories.CompositionGraph
    import Math.FiniteCategories.SafeCompositionGraph
    import Math.FiniteCategories.CommaCategory 
    import Math.FiniteCategories.One
    import Math.FiniteCategories.ConeCategory
    import Math.FiniteCategories.Subcategory
    import Math.FiniteCategories.DiscreteTwo
    import Math.FiniteCategories.LimitCategory
    import Math.FiniteCategories.ColimitCategory
    import Math.FiniteCategories.ExponentialCategory