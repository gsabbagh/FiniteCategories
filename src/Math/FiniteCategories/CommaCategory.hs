{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances #-}

{-| Module  : FiniteCategories
Description : A 'CommaCategory' can be a 'FiniteCategory' if the target category of the diagrams is finite.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

A 'CommaCategory' can be a 'FiniteCategory' if the target category of the diagrams is finite.
-}

module Math.FiniteCategories.CommaCategory 
(
    module Math.Categories.CommaCategory
)
where
    import          Math.Categories.CommaCategory