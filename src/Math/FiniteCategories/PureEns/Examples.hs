{-| Module  : FiniteCategories
Description : An example of  __'PureEns'__.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

An example of  __'PureEns'__.


-}
module Math.FiniteCategories.PureEns.Examples
(
    examplePureEns,
)
where
    import Math.PureSet
    import Data.WeakSet.Safe
    
    import Math.FiniteCategories
    import Math.Categories
    
    examplePureEns :: FullSubCategory PureFinSet
    examplePureEns = pureEns $ set [numberToSet 1, numberToSet 2, numberToSet 3]