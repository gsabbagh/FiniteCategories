{-| Module  : FiniteCategories
Description : Examples of 'FunctorCategory'. Examples of 'Diagram's.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Examples of 'FunctorCategory'. Examples of 'Diagram's.
-}
module Math.FiniteCategories.FunctorCategory.Examples
(
    exampleFunctorCategory,
    exampleDiagramVToSquare,
    exampleDiagramHatToSquare,
    examplePrecomposedFunctorCategory,
    examplePostcomposedFunctorCategory,
    exampleCgdString,
)
where
    import Data.WeakSet.Safe
    import Data.WeakMap.Safe
    
    import Math.FiniteCategory
    import Math.Categories
    import Math.FiniteCategories
    
    import Math.IO.PrettyPrint
    
    import Data.Text (Text)
    
    -- | The 'FunctorCategory' 3^2.
    exampleFunctorCategory :: FunctorCategory NumberCategory NumberCategoryMorphism NumberCategoryObject NumberCategory NumberCategoryMorphism NumberCategoryObject
    exampleFunctorCategory = FunctorCategory (numberCategory 2) (numberCategory 3)
    
    -- | Example of a 'Diagram' from V to Square.
    exampleDiagramVToSquare :: Diagram V VAr VOb Square SquareAr SquareOb
    exampleDiagramVToSquare = completeDiagram Diagram{src=V, tgt=Square, omap=weakMap [], mmap = weakMap [(VF,SquareH),(VG,SquareI)]}
    
    -- | Example of a 'Diagram' from Hat to Square.
    exampleDiagramHatToSquare :: Diagram Hat HatAr HatOb Square SquareAr SquareOb
    exampleDiagramHatToSquare = completeDiagram Diagram{src=Hat, tgt=Square, omap=weakMap [], mmap = weakMap [(HatF,SquareF),(HatG,SquareG)]}
    
    -- | Example of a 'PrecomposedFunctorCategory'.
    examplePrecomposedFunctorCategory :: PrecomposedFunctorCategory V VAr VOb Square SquareAr SquareOb Square SquareAr SquareOb
    examplePrecomposedFunctorCategory = PrecomposedFunctorCategory exampleDiagramVToSquare Square
    
    
    -- | Example of a 'PostcomposedFunctorCategory'.
    examplePostcomposedFunctorCategory :: PostcomposedFunctorCategory Square SquareAr SquareOb V VAr VOb Square SquareAr SquareOb
    examplePostcomposedFunctorCategory = PostcomposedFunctorCategory exampleDiagramVToSquare Square
    
    -- | Example of a 'Diagram' of 'CompositionGraph's constructed by reading a .cgd string.
    exampleCgdString :: Diagram (CompositionGraph Text Text) (CGMorphism Text Text) Text (CompositionGraph Text Text) (CGMorphism Text Text) Text
    Right exampleCgdString = readCGDString $ sourceCG++targetCG++"A -f-> B => 1 -a-> 2\n"
        where
            sourceCG = "<SRC>\nA -f-> B\n</SRC>\n"
            targetCG = "<TGT>\n1 -a-> 2\n1 -b-> 3\n</TGT>\n"