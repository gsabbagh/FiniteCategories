{-| Module  : FiniteCategories
Description : Six examples of 'NumberCategory'.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Six examples of 'NumberCategory'. 
-}
module Math.FiniteCategories.NumberCategory.Examples
(
    exampleNumberCategory0,
    exampleNumberCategory1,
    exampleNumberCategory2,
    exampleNumberCategory3,
    exampleNumberCategory4,
    exampleNumberCategory5,
    exampleDiagramOfNumberCategory,
)
where
    import Math.FiniteCategories.NumberCategory
    import Math.FiniteCategory
    import Math.Categories.FunctorCategory
    import Math.Categories.Omega
    
    import Data.WeakSet
    import Data.WeakMap
    
    import Numeric.Natural
    
    -- | The category 0
    exampleNumberCategory0 :: NumberCategory
    exampleNumberCategory0 = numberCategory 0
    
    -- | The category 1
    exampleNumberCategory1 :: NumberCategory
    exampleNumberCategory1 = numberCategory 1
    
    -- | The category 2
    exampleNumberCategory2 :: NumberCategory
    exampleNumberCategory2 = numberCategory 2
    
    -- | The category 3
    exampleNumberCategory3 :: NumberCategory
    exampleNumberCategory3 = numberCategory 3
    
    
    -- | The category 4
    exampleNumberCategory4 :: NumberCategory
    exampleNumberCategory4 = numberCategory 4
    
    
    -- | The category 5
    exampleNumberCategory5:: NumberCategory
    exampleNumberCategory5 = numberCategory 5
    
    -- | An example of 'Diagram' from 3 to 'Omega'.
    exampleDiagramOfNumberCategory :: Diagram NumberCategory (IsSmallerThan Natural) Natural Omega (IsSmallerThan Natural) Natural
    exampleDiagramOfNumberCategory = diag
        where
            diag = Diagram{src = numberCategory 3, tgt = omega, omap = memorizeFunction id (ob $ numberCategory 3), mmap = memorizeFunction id (arrows $ numberCategory 3)}