{-| Module  : FiniteCategories
Description : A 'FunctorCategory' where the source and target category are finite is a 'FiniteCategory'.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

A 'FunctorCategory' where the source and target category are finite is a 'FiniteCategory'.

-}

module Math.FiniteCategories.FunctorCategory
(
    module Math.Categories.FunctorCategory
)
where
    import          Math.Categories.FunctorCategory
    