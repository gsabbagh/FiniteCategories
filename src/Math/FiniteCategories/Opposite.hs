{-| Module  : FiniteCategories
Description : The 'Op'posite category of a 'FiniteCategory' is a 'FiniteCategory'.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

The 'Op'posite category of a 'FiniteCategory' is a 'FiniteCategory'.
-}

module Math.FiniteCategories.Opposite
(
    module Math.Categories.Opposite
)
where
    import          Math.Categories.Opposite
    