{-| Module  : FiniteCategories
Description : Examples of 'CommaCategory'.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Examples of 'CommaCategory'.
-}
module Math.FiniteCategories.CommaCategory.Examples
(
    exampleSlice,
    exampleCoslice,
    exampleArrowCategory,
)
where
    import Math.Categories
    import Math.FiniteCategories
    
    -- | Example of slice category : (Id_4 | 2). This is the category of objects over 2 in the simplex category 4.
    exampleSlice :: CommaCategory NumberCategory NumberCategoryMorphism NumberCategoryObject One One One NumberCategory NumberCategoryMorphism NumberCategoryObject
    Just exampleSlice = sliceCategory (numberCategory 4) 2
    
    -- | Example of coslice category : (2 | Id_4). This is the category of objects under 2 in the simplex category 4.
    exampleCoslice :: CommaCategory One One One NumberCategory NumberCategoryMorphism NumberCategoryObject NumberCategory NumberCategoryMorphism NumberCategoryObject
    Just exampleCoslice = cosliceCategory (numberCategory 4) 2
    
    -- | Example of arrow category : (Id_4 | Id_4). This is the category of arrows of the simplex category 4.
    exampleArrowCategory :: CommaCategory NumberCategory NumberCategoryMorphism NumberCategoryObject NumberCategory NumberCategoryMorphism NumberCategoryObject NumberCategory NumberCategoryMorphism NumberCategoryObject
    exampleArrowCategory = arrowCategory (numberCategory 4)
    
    