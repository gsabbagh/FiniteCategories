{-| Module  : FiniteCategories
Description : Examples of opposite categories.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Examples of opposite categories.
-}
module Math.FiniteCategories.Opposite.Examples
(
    exampleOppositeEns,
    exampleOppositeNumberCategory,
)
where
    import Data.WeakSet         (powerSet, Set)
    import Data.WeakSet.Safe
    
    import Math.Categories
    import Math.FiniteCategories
    import Math.FiniteCategory
    
    -- | The opposite of an 'Ens' category.
    exampleOppositeEns :: Op (Ens Char)
    exampleOppositeEns = Op (ens.powerSet.set $ "AB")
    
    -- | The opposite of the 'NumberCategory' 4.
    exampleOppositeNumberCategory :: Op NumberCategory
    exampleOppositeNumberCategory = Op (numberCategory 4)