{-| Module  : FiniteCategories
Description : Examples of 'SafeCompositionGraph', some are constructed using the smart constructors, others are random and others are read in a .scg file.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Examples of 'SafeCompositionGraph', some are constructed using the smart constructors, others are random and others are read in a .scg file
-}
module Math.FiniteCategories.SafeCompositionGraph.Examples
(
    exampleSafeCompositionGraph,
    exampleLoopingSafeCompositionGraph,
    exampleScgString,
    exampleRandomSafeCompositionGraph,
)
where
    import qualified Data.WeakSet as Set
    import Data.WeakSet.Safe
    import Data.WeakMap.Safe
    
    import Math.FiniteCategory
    import Math.Categories
    import Math.FiniteCategories

    import Data.Text (Text)

    import System.Random
    
    
    -- | A 'SafeCompositionGraph' constructed using the smart constructor 'safeCompositionGraph'.
    exampleSafeCompositionGraph :: SafeCompositionGraph Int Char
    exampleSafeCompositionGraph = result
        where
            Right result = safeCompositionGraph underlyingGraph compositionLaw 3
            Just underlyingGraph = graph (set [1 :: Int,2,3]) (set [Arrow{sourceArrow=1,targetArrow=1,labelArrow='a'},Arrow{sourceArrow=1,targetArrow=2,labelArrow='b'},Arrow{sourceArrow=2,targetArrow=3,labelArrow='c'}])
            compositionLaw = weakMap [([Arrow{sourceArrow=1,targetArrow=1,labelArrow='a'},Arrow{sourceArrow=1,targetArrow=1,labelArrow='a'}],[Arrow{sourceArrow=1,targetArrow=1,labelArrow='a'}])]
    
    -- | A 'SafeCompositionGraph' containing a loop which would go infinite with a 'CompositionGraph'.
    exampleLoopingSafeCompositionGraph :: SafeCompositionGraph Int Char
    exampleLoopingSafeCompositionGraph = result
        where
            Right result = safeCompositionGraph (unsafeGraph (set [1 :: Int]) (set [Arrow{sourceArrow=1,targetArrow=1,labelArrow='a'}])) (weakMap []) 3
            
    -- | A 'SafeCompositionGraph' read using a .scg string.
    exampleScgString :: SafeCompositionGraph Text Text
    Right exampleScgString = readSCGString "2\nA -f-> B -g-> C = A -h-> C"
    
    -- | A random 'SafeCompositionGraph'.
    exampleRandomSafeCompositionGraph :: SafeCompositionGraph Int Int
    exampleRandomSafeCompositionGraph = result 
        where
            randomGen = mkStdGen 123456
            (result,newGen) = defaultConstructRandomSafeCompositionGraph randomGen