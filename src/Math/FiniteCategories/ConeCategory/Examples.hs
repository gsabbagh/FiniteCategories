{-| Module  : FiniteCategories
Description : Examples of 'ConeCategory'.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Examples of 'ConeCategory' and example of (co)limits computation.
-}
module Math.FiniteCategories.ConeCategory.Examples
(
    exampleConeCategory,
    exampleLimit,
    exampleCoconeCategory,
    exampleColimit,
)
where
    import              Math.Category
    import              Math.FiniteCategories.ConeCategory
    import              Math.FiniteCategories.FunctorCategory
    import              Math.FiniteCategories
    import              Math.FiniteCategories.FunctorCategory.Examples
    
    
    import              Data.WeakSet             (Set)
    import qualified    Data.WeakSet           as Set
    import              Data.WeakSet.Safe
    import              Data.WeakMap             (Map)
    import qualified    Data.WeakMap           as Map
    import              Data.WeakMap.Safe
    
    
    -- | Example of 'ConeCategory'. Here, there are no cone as 'Square.FH' is different from 'Square.GI'.
    exampleConeCategory :: ConeCategory V VAr VOb Square SquareAr SquareOb
    exampleConeCategory = coneCategory exampleDiagramVToSquare
    
    -- | Example of limit computation. Here, there are no limit as 'Square.FH' is different from 'Square.GI'.
    exampleLimit :: Set (Cone V VAr VOb Square SquareAr SquareOb)
    exampleLimit = limits exampleDiagramVToSquare
    
    -- | Example of 'CoconeCategory'. Here, there are no cocone as 'Square.FH' is different from 'Square.GI'.
    exampleCoconeCategory :: CoconeCategory Hat HatAr HatOb Square SquareAr SquareOb
    exampleCoconeCategory = coconeCategory exampleDiagramHatToSquare
    
    -- | Example of colimit computation. Here, there are no colimit as 'Square.FH' is different from 'Square.GI'.
    exampleColimit :: Set (Cocone Hat HatAr HatOb Square SquareAr SquareOb)
    exampleColimit = colimits exampleDiagramHatToSquare
    