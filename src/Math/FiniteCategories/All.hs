{-| Module  : FiniteCategories
Description : ll modules from the FiniteCategories package.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

ll modules from the FiniteCategories package.

ll categories, all finite categories, category and finite category functions, IO, Debug...
-}

module Math.FiniteCategories.All (
    module Math.Category,
    module Math.FiniteCategory,
    module Math.CompleteCategory,
    module Math.CocompleteCategory,
    module Math.CartesianClosedCategory,
    module Math.FiniteCategories.Examples,
    module Math.FiniteCategoryError,
    module Math.Categories,
    module Math.FiniteCategories,
    module Math.Functors,
    module Math.IO.PrettyPrint
) where
    import Math.Category
    import Math.FiniteCategory
    import Math.CompleteCategory
    import Math.CocompleteCategory
    import Math.CartesianClosedCategory
    import Math.FiniteCategories.Examples
    import Math.FiniteCategoryError
    import Math.Categories
    import Math.Functors
    import Math.FiniteCategories
    import Math.IO.PrettyPrint