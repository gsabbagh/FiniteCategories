{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MonadComprehensions  #-}
{-# LANGUAGE MultiParamTypeClasses  #-}

{-| Module  : FiniteCategories
Description : The 'Sketch' of the Haskell programming language.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

The 'Sketch' of the Haskell programming language.

-}

module Math.FiniteCategories.HaskellSketch
(
    sketchBool,
    sketchEnrichedBool,
)
where
    import              Data.WeakSet             (Set)
    import qualified    Data.WeakSet           as Set
    import              Data.WeakSet.Safe
    import              Data.WeakMap             (Map)
    import qualified    Data.WeakMap           as Map
    import              Data.WeakMap.Safe
    import              Data.Simplifiable
    import              Data.Text               (Text,unpack, pack)
    import              Data.List               (intercalate)
    
    import              Math.Categories
    import              Math.FiniteCategories.CompositionGraph
    
    
    -- | The 'Sketch' of the 'Bool' type in Haskell.
    sketchBool :: Sketch Text Text
    sketchBool = unsafeSketch cg (set [cone2]) (set [cocone3])
        where
            cg = simplify $ unsafeReadCGString "Bool\n1\n1 -True-> Bool\n1 -False-> Bool\n"
            indexingCG2 = simplify $ unsafeReadCGString ""
            base2 = simplify $ constantDiagram indexingCG2 cg undefined
            apex2 = (pack "1")
            cone2 = simplify $ unsafeCone apex2 (unsafeNaturalTransformation (constantDiagram (src base2) (tgt base2) apex2) base2 (weakMap []))
            indexingCG3 = simplify $ unsafeReadCGString "A\nB\n"
            base3 = simplify $ constantDiagram indexingCG3 cg (pack "1")
            nadir3 = pack "Bool"
            cocone3 = simplify $ unsafeCocone nadir3 (unsafeNaturalTransformation base3 (constantDiagram (src base3) (tgt base3) nadir3) (weakMap [(pack "A", unsafeGetMorphismFromLabel cg (pack "False")),(pack "B", unsafeGetMorphismFromLabel cg (pack "True"))]))
    
    -- | The 'Sketch' of the 'Bool' type enriched with several function to ease the programmation in Falcon.
    sketchEnrichedBool :: Sketch Text Text
    sketchEnrichedBool = unsafeSketch cg (set [cone1, cone2]) (set [cocone3])
        where
            cg = simplify $ unsafeReadCGString "Bool x Bool\nBool\n1\n\n1 -True-> Bool\n1 -False-> Bool\nBool x Bool -p_1-> Bool\nBool x Bool -p_2-> Bool\nBool x Bool -&&-> Bool\nBool x Bool -||-> Bool\nBool x Bool -equal-> Bool\n\nBool -not-> Bool -not-> Bool = <ID>\n\n1 -True,True-> Bool x Bool -p_1-> Bool = 1 -True-> Bool\n1 -True,True-> Bool x Bool -p_2-> Bool = 1 -True-> Bool\n1 -True,True-> Bool x Bool -&&-> Bool = 1 -True-> Bool\n1 -True,True-> Bool x Bool -||-> Bool = 1 -True-> Bool\n1 -True,True-> Bool x Bool -equal-> Bool = 1 -True-> Bool\n\n1 -False,True-> Bool x Bool -p_1-> Bool = 1 -False-> Bool\n1 -False,True-> Bool x Bool -p_2-> Bool = 1 -True-> Bool\n1 -False,True-> Bool x Bool -&&-> Bool = 1 -False-> Bool\n1 -False,True-> Bool x Bool -||-> Bool = 1 -True-> Bool\n1 -False,True-> Bool x Bool -equal-> Bool = 1 -False-> Bool\n\n1 -True,False-> Bool x Bool -p_1-> Bool = 1 -True-> Bool\n1 -True,False-> Bool x Bool -p_2-> Bool = 1 -False-> Bool\n1 -True,False-> Bool x Bool -&&-> Bool = 1 -False-> Bool\n1 -True,False-> Bool x Bool -||-> Bool = 1 -True-> Bool\n1 -True,False-> Bool x Bool -equal-> Bool = 1 -False-> Bool\n\n1 -False,False-> Bool x Bool -p_1-> Bool = 1 -False-> Bool\n1 -False,False-> Bool x Bool -p_2-> Bool = 1 -False-> Bool\n1 -False,False-> Bool x Bool -&&-> Bool = 1 -False-> Bool\n1 -False,False-> Bool x Bool -||-> Bool = 1 -False-> Bool\n1 -False,False-> Bool x Bool -equal-> Bool = 1 -True-> Bool\n"
            indexingCG1 = simplify $ unsafeReadCGString "A\nB\n"
            base1 = simplify $ constantDiagram indexingCG1 cg (pack "Bool")
            apex1 = (pack "Bool x Bool")
            cone1 = simplify $ unsafeCone apex1 (unsafeNaturalTransformation (constantDiagram (src base1) (tgt base1) apex1) base1 (weakMap [(pack "A", unsafeGetMorphismFromLabel cg (pack "p_1")),(pack "B", unsafeGetMorphismFromLabel cg (pack "p_2"))]))
            indexingCG2 = simplify $ unsafeReadCGString ""
            base2 = simplify $ constantDiagram indexingCG2 cg undefined
            apex2 = (pack "1")
            cone2 = simplify $ unsafeCone apex2 (unsafeNaturalTransformation (constantDiagram (src base2) (tgt base2) apex2) base2 (weakMap []))
            indexingCG3 = simplify $ unsafeReadCGString "A\nB\n"
            base3 = simplify $ constantDiagram indexingCG3 cg (pack "1")
            nadir3 = pack "Bool"
            cocone3 = simplify $ unsafeCocone nadir3 (unsafeNaturalTransformation base3 (constantDiagram (src base3) (tgt base3) nadir3) (weakMap [(pack "A", unsafeGetMorphismFromLabel cg (pack "False")),(pack "B", unsafeGetMorphismFromLabel cg (pack "True"))]))
            
    -- | The Sketch of the 'Int8' type.
    -- sketchInt8 :: Sketch Text Text
    -- sketchInt8 = 
        -- where
            -- ppInt i = if i >= 0 then (show i) else ("_" ++ (show (-i)))
            -- cg = simplify $ unsafeReadCGString  $ "1 -0-> Int8\nInt8 -s-> Int8\n"++ intercalate "\n" ["1 -"++ ppInt i ++"-> Int8 -s-> Int8 = 1 -" ++ ppInt (if i == 127  then -128 else i+1) ++ "-> Int8" | i <- [-128..127]] ++ "\n" ++ intercalate " -s-> " (take 257 (repeat "Int8")) ++ " = <ID>\n"
            