{-| Module  : FiniteCategories
Description : Export all finite category examples.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Export all finite category examples. 
-}

module Math.FiniteCategories.Examples
(
    module Math.FiniteCategories.CommaCategory.Examples,
    module Math.FiniteCategories.CompositionGraph.Examples,
    module Math.FiniteCategories.ConeCategory.Examples,
    module Math.FiniteCategories.DiscreteCategory.Examples,
    module Math.FiniteCategories.Ens.Examples,
    module Math.FiniteCategories.FinCat.Examples,
    module Math.FiniteCategories.FinGrph.Examples,
    module Math.FiniteCategories.FinSketch.Examples,
    module Math.FiniteCategories.FunctorCategory.Examples,
    module Math.FiniteCategories.LimitCategory.Examples,
    module Math.FiniteCategories.NumberCategory.Examples,
    module Math.FiniteCategories.Opposite.Examples,
    module Math.FiniteCategories.SafeCompositionGraph.Examples,
)
where
    import Math.FiniteCategories.CommaCategory.Examples
    import Math.FiniteCategories.CompositionGraph.Examples
    import Math.FiniteCategories.ConeCategory.Examples
    import Math.FiniteCategories.DiscreteCategory.Examples
    import Math.FiniteCategories.Ens.Examples
    import Math.FiniteCategories.FinCat.Examples
    import Math.FiniteCategories.FinGrph.Examples
    import Math.FiniteCategories.FinSketch.Examples
    import Math.FiniteCategories.FunctorCategory.Examples
    import Math.FiniteCategories.LimitCategory.Examples
    import Math.FiniteCategories.NumberCategory.Examples
    import Math.FiniteCategories.Opposite.Examples
    import Math.FiniteCategories.SafeCompositionGraph.Examples
