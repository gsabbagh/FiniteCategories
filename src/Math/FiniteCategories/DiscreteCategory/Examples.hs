{-| Module  : FiniteCategories
Description : Six examples of 'DiscreteCategory'.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Six examples of 'DiscreteCategory'.

Categories __0__ up to __5__ .
-}
module Math.FiniteCategories.DiscreteCategory.Examples
(
    exampleDiscreteCategory0,
    exampleDiscreteCategory1,
    exampleDiscreteCategory2,
    exampleDiscreteCategory3,
    exampleDiscreteCategory4,
    exampleDiscreteCategory5,
)
where
    import Data.WeakSet.Safe
    
    import Math.FiniteCategories.DiscreteCategory
    
    -- | An example of 'DiscreteCategory' with 0 object.
    exampleDiscreteCategory0 :: DiscreteCategory Int
    exampleDiscreteCategory0 = discreteCategory (set [])
    
    -- | An example of 'DiscreteCategory' with 1 object.
    exampleDiscreteCategory1 :: DiscreteCategory Int
    exampleDiscreteCategory1 = discreteCategory (set [1])
    
    -- | An example of 'DiscreteCategory' with 2 objects.
    exampleDiscreteCategory2 :: DiscreteCategory Int
    exampleDiscreteCategory2 = discreteCategory (set [1..2])
    
    -- | An example of 'DiscreteCategory' with 3 objects.
    exampleDiscreteCategory3 :: DiscreteCategory Int
    exampleDiscreteCategory3 = discreteCategory (set [1..3])
    
    -- | An example of 'DiscreteCategory' with 4 objects.
    exampleDiscreteCategory4 :: DiscreteCategory Int
    exampleDiscreteCategory4 = discreteCategory (set [1..4])
    
    -- | An example of 'DiscreteCategory' with 5 objects.
    exampleDiscreteCategory5 :: DiscreteCategory Int
    exampleDiscreteCategory5 = discreteCategory (set [1..5])