{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances #-}

{-| Module  : FiniteCategories
Description : A 'ConeCategory' can be a 'FiniteCategory' if the target category of the diagrams is finite.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

A 'ConeCategory' can be a 'FiniteCategory' if the target category of the diagrams is finite.
-}

module Math.FiniteCategories.ConeCategory 
(
    module Math.Categories.ConeCategory
)
where
    import          Math.Categories.ConeCategory