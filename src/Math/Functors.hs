{-# LANGUAGE MultiParamTypeClasses #-}

{-| Module  : FiniteCategories
Description : This file exports all functors.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

This file exports all functors.
-}

module Math.Functors (
    module Math.Functors.Adjunction,
    module Math.Functors.DataMigration,
    module Math.Functors.DiagonalFunctor,
    module Math.Functors.KanExtension,
    module Math.Functors.SetValued,
) where
    import Math.Functors.Adjunction
    import Math.Functors.DataMigration
    import Math.Functors.DiagonalFunctor
    import Math.Functors.KanExtension
    import Math.Functors.SetValued