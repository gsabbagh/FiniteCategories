{-| Module  : FiniteCategories
Description : This file exports all Functors examples.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

This file exports all Functors examples.
-}

module Math.Functors.Examples
(
    module Math.Functors.Adjunction.Examples,
    module Math.Functors.DataMigration.Examples,
    module Math.Functors.DiagonalFunctor.Examples,
    module Math.Functors.KanExtension.Examples,
    module Math.Functors.SetValued.Examples,
    module Math.Functors.YonedaEmbedding.Examples,
)
where
    import Math.Functors.Adjunction.Examples
    import Math.Functors.DataMigration.Examples
    import Math.Functors.DiagonalFunctor.Examples
    import Math.Functors.KanExtension.Examples
    import Math.Functors.SetValued.Examples
    import Math.Functors.YonedaEmbedding.Examples