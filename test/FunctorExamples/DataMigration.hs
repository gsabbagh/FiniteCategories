{-| Module  : FiniteCategories
Description : Examples of data migration functors pretty printed.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Examples of data migration functors pretty printed.
-}
module FunctorExamples.DataMigration
(
    test
)
where
    import              Math.Functors.Examples
    import              Math.IO.PrettyPrint
    import              Math.FiniteCategory
    
    -- | Examples of data migration functors pretty printed.
    test :: IO ()
    test = do
        putStrLn "Start of FunctorExamples.DataMigration"
        pp 3 exampleDeltaFunctor
        pp 3 examplePiFunctor
        pp 3 exampleSigmaFunctor
        putStrLn "End of FunctorExamples.DataMigration"