{-| Module  : FiniteCategories
Description : Examples of Kan extensions of set-valued functors pretty printed.
Copyright   : Guillaume Sabbagh 2023
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Examples of Kan extensions of set-valued functors pretty printed.


-}
module FunctorExamples.SetValued
(
    test
)
where
    import              Math.FiniteCategory
    import              Math.IO.PrettyPrint
    import              Math.Functors.Examples
    

    
    -- | Examples of Kan extensions of set-valued functors pretty printed.
    test :: IO ()
    test = do
        putStrLn "Start of FunctorExamples.SetValued"
        pp 2 exampleSetValuedLeftKanExtension
        pp 2 exampleSetValuedRightKanExtension
        putStrLn "End of FunctorExamples.SetValued"