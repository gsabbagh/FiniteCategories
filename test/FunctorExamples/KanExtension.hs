{-| Module  : FiniteCategories
Description : Examples of Kan extensions pretty printed.
Copyright   : Guillaume Sabbagh 2023
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Examples of Kan extensions pretty printed.


-}
module FunctorExamples.KanExtension
(
    test
)
where
    import              Math.FiniteCategory
    import              Math.Functors.Examples
    import              Math.IO.PrettyPrint
    
    -- | Examples of Kan extensions pretty printed.
    test :: IO ()
    test = do
        putStrLn "Start of FunctorExamples.KanExtension"
        pp 2 exampleLeftKanExtension
        pp 2 exampleRightKanExtension
        putStrLn "End of FunctorExamples.KanExtension"