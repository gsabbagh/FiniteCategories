{-| Module  : FiniteCategories
Description : An example of a 'diagonalFunctor' pretty printed.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

An example of 'diagonalFunctor' pretty printed.


-}
module FunctorExamples.DiagonalFunctor
(
    test
)
where
    import              Math.Functors.Examples
    import              Math.IO.PrettyPrint
    import              Math.FiniteCategory
    
    -- | An example of 'diagonalFunctor' pretty printed.
    test :: IO ()
    test = do
        putStrLn "Start of FunctorExamples.DiagonalFunctor"
        pp 2 exampleDiagonalFunctor
        putStrLn "End of FunctorExamples.DiagonalFunctor"