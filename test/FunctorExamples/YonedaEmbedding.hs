{-| Module  : FiniteCategories
Description : Examples of 'yonedaEmbedding' pretty printed.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Examples of  'yonedaEmbedding' pretty printed.


-}
module FunctorExamples.YonedaEmbedding
(
    test
)
where
    
    import              Math.FiniteCategory
    import              Math.Functors.Examples
    import              Math.IO.PrettyPrint
    
    -- | 'yonedaEmbedding's pretty printed.
    test :: IO ()
    test = do
        putStrLn "Start of FunctorExamples.YonedaEmbedding"
        pp 2 exampleYonedaEmbeddingOfHat
        pp 2 exampleYonedaEmbeddingOfSquare
        putStrLn "End of FunctorExamples.YonedaEmbedding"