{-| Module  : FiniteCategories
Description : An exemple of 'leftAdjoint' and 'rightAdjoint' use pretty printed.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

An exemple of 'leftAdjoint' and 'rightAdjoint' use pretty printed.


-}
module FunctorExamples.Adjunction
(
    test
)
where
    import              Math.Functors.Examples
    import              Math.IO.PrettyPrint
    import              Math.FiniteCategory
    
    -- | An exemple of 'leftAdjoint' and 'rightAdjoint' use pretty printed.
    test :: IO ()
    test = do
        putStrLn "Start of FunctorExamples.Adjunction"
        pp 5 exampleLeftAdjoint
        pp 5 exampleRightAdjoint
        putStrLn "End of FunctorExamples.Adjunction"