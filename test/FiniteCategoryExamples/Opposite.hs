{-| Module  : FiniteCategories
Description : Examples of opposite categories pretty printed.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Examples of opposite categories pretty printed.


-}
module FiniteCategoryExamples.Opposite
(
    test
)
where
    import Math.FiniteCategories.Examples
    import Math.FiniteCategory
    import Math.IO.PrettyPrint
    
    
    -- | Examples of opposite categories pretty printed.
    test :: IO ()
    test = do
        putStrLn "Start of FiniteCategoryExamples.Opposite"
        putStrLn $  pprintFiniteCategory 2 exampleOppositeEns 
        putStrLn $  pprintFiniteCategory 3 exampleOppositeNumberCategory 
        putStrLn "End of FiniteCategoryExamples.Opposite"