{-| Module  : FiniteCategories
Description : Examples of 'FunctorCategory' pretty printed.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Examples of 'FunctorCategory' pretty printed.


-}
module FiniteCategoryExamples.FunctorCategory
(
    test
)
where
    import Math.FiniteCategories.FunctorCategory.Examples
    import Math.FiniteCategory
    import Math.IO.PrettyPrint
    
    -- | Examples of 'FunctorCategory' pretty printed.
    test :: IO ()
    test = do
        putStrLn "Start of FiniteCategoryExamples.FunctorCategory"
        putStrLn $  pprintFiniteCategory 2 exampleFunctorCategory
        putStrLn $  pprint 2 exampleDiagramVToSquare
        putStrLn $  pprint 2 exampleDiagramHatToSquare
        putStrLn $  pprintFiniteCategory 2 examplePrecomposedFunctorCategory
        putStrLn $  pprintFiniteCategory 2 examplePostcomposedFunctorCategory
        putStrLn $  pprint 2 exampleCgdString
        putStrLn "End of FiniteCategoryExamples.FunctorCategory"