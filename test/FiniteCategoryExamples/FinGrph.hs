{-| Module  : FiniteCategories
Description : An example of 'FullSubcategory' of __'FinGrph'__  and a 'Graph' pretty printed.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

An example of 'FullSubcategory' of __'FinGrph'__ and a 'Graph' pretty printed.


-}
module FiniteCategoryExamples.FinGrph
(
    test
)
where
    import              Math.FiniteCategories.Examples
    import              Math.IO.PrettyPrint
    import              Math.FiniteCategory
    
    
    -- | An example of 'FullSubcategory' of __'FinGrph'__ and a 'Graph' pretty printed.
    test :: IO ()
    test = do
        putStrLn "Start of FiniteCategoryExamples.FinGrph"
        pp 1 exampleGraph
        putStrLn $ pprintFiniteCategory 2 exampleFinGrph
        putStrLn "End of FiniteCategoryExamples.FinGrph"