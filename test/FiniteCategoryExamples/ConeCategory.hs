{-| Module  : FiniteCategories
Description : Examples of 'ConeCategory' pretty printed.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Examples of 'ConeCategory' pretty printed.
-}
module FiniteCategoryExamples.ConeCategory
(
    test
)
where
    import              Math.FiniteCategories.ConeCategory.Examples
    import              Math.FiniteCategory
    import              Math.IO.PrettyPrint
    
    -- | Examples of 'ConeCategory' pretty printed.
    test :: IO ()
    test = do
        putStrLn "Start of FiniteCategoryExamples.ConeCategory"
        putStrLn $ pprintFiniteCategory 3 exampleConeCategory
        putStrLn $ pprintFiniteCategory 3 exampleCoconeCategory
        putStrLn.show $ pprint 2 <$> exampleLimit
        putStrLn.show $ pprint 2 <$> exampleColimit
        putStrLn "End of FiniteCategoryExamples.ConeCategory"