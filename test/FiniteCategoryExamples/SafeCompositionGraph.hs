{-| Module  : FiniteCategories
Description : Examples of 'SafeCompositionGraph', some are constructed using the smart constructors, others are random and others are read in a .scg file are pretty printed.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Examples of 'SafeCompositionGraph', some are constructed using the smart constructors, others are random and others are read in a .scg file are pretty printed.
-}
module FiniteCategoryExamples.SafeCompositionGraph
(
    test
)
where
    import Math.FiniteCategory
    import Math.Categories
    import Math.FiniteCategories.Examples
    
    import Math.IO.PrettyPrint
    
    -- | Examples of 'SafeCompositionGraph', some are constructed using the smart constructors, others are random and others are read in a .scg file are pretty printed.
    test :: IO ()
    test = do
        putStrLn "Start of FiniteCategoryExamples.SafeCompositionGraph"
        putStrLn $  pprintFiniteCategory 2 exampleSafeCompositionGraph
        putStrLn $  pprintFiniteCategory 2 exampleLoopingSafeCompositionGraph
        putStrLn $  pprintFiniteCategory 2 exampleScgString
        putStrLn $  pprintFiniteCategory 2 exampleRandomSafeCompositionGraph
        putStrLn "End of FiniteCategoryExamples.SafeCompositionGraph"