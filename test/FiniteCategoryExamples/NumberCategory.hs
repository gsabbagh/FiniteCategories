{-| Module  : FiniteCategories
Description : Six examples of 'NumberCategory' pretty printed.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Six examples of 'NumberCategory' pretty printed. 
-}
module FiniteCategoryExamples.NumberCategory
(
    test
)
where
    import Math.FiniteCategories.Examples
    import Math.IO.PrettyPrint
    import Math.FiniteCategory
    
    
    -- | Six examples of 'NumberCategory' pretty printed.
    test :: IO ()
    test = do
        putStrLn "Start of FiniteCategoryExamples.NumberCategory"
        putStrLn $ pprintFiniteCategory 2 $ exampleNumberCategory0
        putStrLn $ pprintFiniteCategory 2 $ exampleNumberCategory1
        putStrLn $ pprintFiniteCategory 2 $ exampleNumberCategory2
        putStrLn $ pprintFiniteCategory 2 $ exampleNumberCategory3
        putStrLn $ pprintFiniteCategory 2 $ exampleNumberCategory4
        putStrLn $ pprintFiniteCategory 2 $ exampleNumberCategory5
        putStrLn "End of FiniteCategoryExamples.NumberCategory"