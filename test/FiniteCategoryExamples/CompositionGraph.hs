{-| Module  : FiniteCategories
Description : Examples of 'CompositionGraph' pretty printed.
Copyright   : Guillaume Sabbagh 2023
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Examples of 'CompositionGraph's pretty printed.
-}
module FiniteCategoryExamples.CompositionGraph
(
    test
)
where
    import Math.FiniteCategories.CompositionGraph.Examples
    import Math.FiniteCategory
    import Math.FiniteCategories.ConeCategory
    import Math.IO.PrettyPrint
    
    
    -- | Examples of 'CompositionGraph' pretty printed.
    test :: IO ()
    test = do
        putStrLn "Start of FiniteCategoryExamples.CompositionGraph"
        putStrLn $ pprintFiniteCategory 2 $ exampleCompositionGraph
        pp 2 exampleSetTransformedIntoACompositionGraph
        pp 3 example4TransformedIntoACompositionGraph
        putStrLn $ pprintFiniteCategory 2 $ exampleRandomCompositionGraph
        putStrLn $ pprintFiniteCategory 2 $ exampleCgString
        putStrLn $ pprintFiniteCategory 2 $ exampleCgString2
        putStrLn $ pprintFiniteCategory 2 $ nadir $ exampleColimitOfCompositionGraphs
        putStrLn "End of FiniteCategoryExamples.CompositionGraph"