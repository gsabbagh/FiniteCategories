{-| Module  : FiniteCategories
Description : Examples of 'CommaCategory' pretty printed.
Copyright   : Guillaume Sabbagh 2023
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Examples of 'CommaCategory' pretty printed.
-}
module FiniteCategoryExamples.CommaCategory
(
    test
)
where
    import Math.FiniteCategory
    import Math.FiniteCategories.CommaCategory.Examples
    import Math.IO.PrettyPrint
    
    -- | Examples of 'CommaCategory' pretty printed.
    test :: IO ()
    test = do
        putStrLn "Start of FiniteCategoryExamples.CommaCategory"
        putStrLn $ pprintFiniteCategory 2 exampleSlice
        putStrLn $ pprintFiniteCategory 2 exampleCoslice
        putStrLn $ pprintFiniteCategory 2 exampleArrowCategory
        putStrLn "End of FiniteCategoryExamples.CommaCategory"