{-| Module  : FiniteCategories
Description : Six examples of 'DiscreteCategory' pretty printed.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Six examples of 'DiscreteCategory' pretty printed.

Pretty print categories __0__ up to __5__ .
-}
module FiniteCategoryExamples.DiscreteCategory
(
    test
)
where
    import Math.FiniteCategories.DiscreteCategory.Examples
    import Math.IO.PrettyPrint
    import Math.FiniteCategory
    
    -- | Six examples of 'DiscreteCategory' pretty printed.
    test :: IO ()
    test = do
        putStrLn "Start of FiniteCategoryExamples.DiscreteCategory"
        putStrLn $ pprintFiniteCategory 1 exampleDiscreteCategory0
        putStrLn $ pprintFiniteCategory 1 exampleDiscreteCategory1
        putStrLn $ pprintFiniteCategory 1 exampleDiscreteCategory2
        putStrLn $ pprintFiniteCategory 1 exampleDiscreteCategory3
        putStrLn $ pprintFiniteCategory 1 exampleDiscreteCategory4
        putStrLn $ pprintFiniteCategory 1 exampleDiscreteCategory5
        putStrLn "End of FiniteCategoryExamples.DiscreteCategory"