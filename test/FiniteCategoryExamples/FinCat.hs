{-| Module  : FiniteCategories
Description : An example of 'FullSubcategory' of __'FinCat'__ pretty printed.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

An example of 'FullSubcategory' of __'FinCat'__ pretty printed.
-}
module FiniteCategoryExamples.FinCat
(
    test
)
where
    import Data.WeakSet.Safe
    
    import Math.Categories
    import Math.FiniteCategories.Examples
    import Math.IO.PrettyPrint
    import Math.FiniteCategory

    
    -- | An example of 'FullSubcategory' of __'FinCat'__ pretty printed.
    test :: IO ()
    test = do
        putStrLn "Start of FiniteCategoryExamples.FinCat"
        putStrLn $ pprintFiniteCategory 1 exampleFinCatNumbers
        putStrLn "End of FiniteCategoryExamples.FinCat"