{-| Module  : FiniteCategories
Description : Examples of 'LimitCategory' pretty printed.
Copyright   : Guillaume Sabbagh 2023
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Examples of 'LimitCategory' pretty printed.
-}
module FiniteCategoryExamples.LimitCategory
(
    test
)
where
    import Math.FiniteCategories.LimitCategory.Examples
    import Math.FiniteCategory
    import Math.FiniteCategories.ConeCategory
    import Math.IO.PrettyPrint
    
    
    -- | Examples of 'LimitCategory' pretty printed.
    test :: IO ()
    test = do
        putStrLn "Start of FiniteCategoryExamples.LimitCategory"
        putStrLn $ pprintFiniteCategory 2 $ exampleLimitCategoryOfCompositionGraphs
        putStrLn $ pprintFiniteCategory 2 $ apex $ exampleLimitOfCompositionGraphs
        putStrLn "End of FiniteCategoryExamples.LimitCategory"