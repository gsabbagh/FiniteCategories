{-| Module  : FiniteCategories
Description : Examples of  __'Ens'__ pretty printed.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Examples of  __'Ens'__ pretty printed.
-}
module FiniteCategoryExamples.Ens
(
    test
)
where
    import Math.FiniteCategories.Ens.Examples
    import Math.IO.PrettyPrint
    import Math.FiniteCategory
    import Math.Categories.ConeCategory
    
    -- | __'Ens'__ pretty printed.
    test :: IO ()
    test = do
        putStrLn "Start of Math.FiniteCategoryExamples.Ens"
        putStrLn $ pprintFiniteCategory 2 exampleEns
        putStrLn $ pprintFiniteCategory 2 exampleEns2
        pp 2 $ apex $ exampleLimitSet
        pp 2 $ nadir $ exampleColimitSet
        putStrLn "End of Math.FiniteCategoryExamples.Ens"