{-| Module  : FiniteCategories
Description : Run all examples of the project.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Run all examples of the project. See results in the folder OutputGraphViz.
-}

module Main
(
    main
)
where
    import qualified FiniteCategoryExamples.CommaCategory           as  CommaCategory
    import qualified FiniteCategoryExamples.CompositionGraph        as  CompositionGraph
    import qualified FiniteCategoryExamples.ConeCategory            as  ConeCategory
    import qualified FiniteCategoryExamples.DiscreteCategory        as  DiscreteCategory
    import qualified FiniteCategoryExamples.Ens                     as  Ens
    import qualified FiniteCategoryExamples.FinCat                  as  FinCat
    import qualified FiniteCategoryExamples.FinGrph                 as  FinGrph
    import qualified FiniteCategoryExamples.FunctorCategory         as  FunctorCategory
    import qualified FiniteCategoryExamples.LimitCategory           as  LimitCategory
    import qualified FiniteCategoryExamples.NumberCategory          as  NumberCategory
    import qualified FiniteCategoryExamples.Opposite                as  Opposite
    import qualified FiniteCategoryExamples.SafeCompositionGraph    as  SafeCompositionGraph
    import qualified FunctorExamples.Adjunction                     as  Adjunction
    import qualified FunctorExamples.DataMigration                  as  DataMigration
    import qualified FunctorExamples.DiagonalFunctor                as  DiagonalFunctor
    import qualified FunctorExamples.KanExtension                   as  KanExtension
    import qualified FunctorExamples.SetValued                      as  SetValued
    import qualified FunctorExamples.YonedaEmbedding                as  YonedaEmbedding
    import qualified CheckAllFiniteCategories                       as  CHECK
    
    -- | Run all examples of the project. See results in the folder OutputGraphViz.
    main = do
        let spacing = 5
        let printSpacing = putStrLn $ take spacing (repeat '\n')
        CommaCategory.test
        printSpacing
        CompositionGraph.test
        printSpacing
        ConeCategory.test
        printSpacing
        DiscreteCategory.test
        printSpacing
        Ens.test
        printSpacing
        FinCat.test
        printSpacing
        FunctorCategory.test
        printSpacing
        FinGrph.test
        printSpacing
        LimitCategory.test
        printSpacing
        NumberCategory.test
        printSpacing
        Opposite.test
        printSpacing
        SafeCompositionGraph.test
        printSpacing
        Adjunction.test
        printSpacing
        DataMigration.test
        printSpacing
        DiagonalFunctor.test
        printSpacing
        KanExtension.test
        printSpacing
        SetValued.test
        printSpacing
        YonedaEmbedding.test
        printSpacing
        CHECK.test