{-| Module  : FiniteCategories
Description : Check the structure of every finite categories defined in this package.
Copyright   : Guillaume Sabbagh 2021
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Check the structure of every finite categories defined in this package.
-}
module CheckAllFiniteCategories
(
    test
)
where
    import              Math.FiniteCategory
    import              Math.FiniteCategoryError
    import              Math.Categories
    import              Math.FiniteCategories
    import              Math.FiniteCategories.Examples
    
    import              Data.WeakSet            (Set)    
    import qualified    Data.WeakSet        as  Set    
    import              Data.WeakSet.Safe
    import              Data.WeakMap           (Map)    
    import qualified    Data.WeakMap        as  Map    
    import              Data.WeakMap.Safe
    import              Data.Text               (Text, pack)
    import              Data.Maybe              (fromJust)
    
    import              Numeric.Natural

    assert :: (Show c, Show m, Show o, Eq m, Eq o, FiniteCategory c m o, Morphism m o) => c -> IO ()
    assert cat 
        | null check = putStrLn $ (show cat)++" passed"
        | otherwise =   do 
                            putStrLn $ (show cat)++" failed: "++(show err)
                            error "Assertion failed"
        where
            check = checkFiniteCategory cat
            Just err = check
    
    -- | Check the structure of every finite categories defined in this package.
    test = do
            putStrLn "Start of CheckAllFiniteCategories"
            assert V
            assert Hat
            assert Parallel
            assert Square
            assert One
            assert DiscreteTwo
            assert exampleSlice
            assert exampleCoslice
            assert exampleArrowCategory
            assert exampleCompositionGraph
            assert $ tgt exampleSetTransformedIntoACompositionGraph
            assert $ tgt example4TransformedIntoACompositionGraph
            assert exampleRandomCompositionGraph
            assert exampleCgString
            assert exampleCgString2
            assert exampleConeCategory
            assert exampleCoconeCategory
            assert exampleDiscreteCategory0
            assert exampleDiscreteCategory1
            assert exampleDiscreteCategory2
            assert exampleDiscreteCategory3
            assert exampleDiscreteCategory4
            assert exampleDiscreteCategory5
            assert exampleEns
            assert exampleEns2
            assert exampleFinCatNumbers
            assert exampleFinGrph
            assert exampleFunctorCategory
            assert examplePrecomposedFunctorCategory
            assert examplePostcomposedFunctorCategory
            assert exampleNumberCategory0
            assert exampleNumberCategory1
            assert exampleNumberCategory2
            assert exampleNumberCategory3
            assert exampleNumberCategory4
            assert exampleNumberCategory5
            assert exampleOppositeEns
            assert exampleOppositeNumberCategory
            assert exampleSafeCompositionGraph
            assert exampleLoopingSafeCompositionGraph
            assert exampleScgString
            assert exampleRandomSafeCompositionGraph
            assert exampleLimitCategoryOfCompositionGraphs
            -- assert $ (embeddingToSubcategory.yonedaEmbedding $ Square)
            putStrLn "End of CheckAllFiniteCategories"